/*
 * Ribbon printer used to display ribbons on top of image elements.Requires
 * HTML5 Canvas support form browser.
 * 
 * @param String imgSrc : image to be displayed
 * @param String [] text : texts to be displayed in the ribbon
 */

(function( $ ) {
    $.fn.ribbon = function( options ) {
    	
    	var settings = $.extend({
			'ribbonColor': '#F96655',
			'ribbonSize' : 30,
			'textColor': 'black',
			'fontSize' : 14,
			'fontFamily' : 'Arial',
			'textStyle' : '18px Arial',
			'sort' : true,
			'angle' : 45,
			'texts' : []
			
		}, options),
			self = this;
		
		this.bubbleSort = function   (arr) {
			var length = arr.length,
				nLength = 0,
				tmpValue = null,
				i;
			
			if(length > 1) {
				
				while(length > 0) {
					nLength = 0;
					
					for(i = 0; i < length-1; i++) {
						
						// Swap
						if(arr[i].length > arr[i+1].length) {
							tmpValue = arr[i];
							arr[i] = arr[i+1];
							arr[i+1] = tmpValue;
							nLength = i;
						}
					}
					length = nLength;
				}
			}
			return arr;
		};
		
		return this.each( function() {
			
			var img = new Image(),
			    ctx = this.getContext('2d'),
			    // We need radians
			    rAngle = settings.angle * (Math.PI / 180),
			    sortedTexts,
				textLength,
				bannerEnd,
				altitude;
			
			// Sort the array from shortest to the longest text
			if($.isArray(settings.texts) && settings.sort) {
				sortedTexts = self.bubbleSort(settings.texts);
			} else {
				console.error('"settings.texts" not an array. exiting...');
				return false;
			}
			
			img.onload = function() {
				// Draw the image once it is loaded
				ctx.drawImage(img, 0, 0);
				// Format the font
				ctx.font = ''+settings.fontSize+'px '+settings.fontFamily;
				ctx.fillStyle = settings.textColor;
				// Tie the ribbon size to the font size used
				settings.ribbonSize = settings.fontSize + settings.fontSize / 2;
				
				// Begin drawing the 
				$.each(sortedTexts, function(idx, elem) {
					ctx.beginPath();
					
					textLength = ctx.measureText(elem).width;
					
					// We have to prepare for too short texts
					if(textLength < 45) {
						textLength = 45;
					}
					
					// Distance from the left top corner to the lowest/highest point of the banner
					bannerEnd = textLength / Math.acos(45 * (Math.PI / 180));
					// Altitude from the left top corner to the middle of the text
					altitude = (textLength/2) / Math.atan(45 * (Math.PI / 180));
					
					ctx.rotate(-rAngle);
					ctx.moveTo(-bannerEnd, altitude);
					ctx.lineTo(bannerEnd, altitude);
					ctx.lineWidth = settings.ribbonSize;
					ctx.strokeStyle = settings.ribbonColor;
					ctx.stroke();
					
					ctx.textAlign = "center";
					ctx.fillText(elem,0,altitude+(settings.fontSize/4));
					ctx.rotate(rAngle);
				});
				
			};
			
			img.src = settings.imgSrc;
		});		
    };
})( jQuery );